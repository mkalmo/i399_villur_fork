export class Contact {
    done: boolean = false;

    constructor(public name: string, public phone: string,public _id: string) {
    };
}

export class NewContact {
    constructor(public name: string, public phone: string) {}
}