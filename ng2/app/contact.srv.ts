import { Http, Response } from "@angular/http";
import { Injectable } from "@angular/core";
import {Contact, NewContact} from "./contact.cls";


@Injectable()
export class ContactService {
    constructor(private http: Http) {}

    getContacts(): Promise<Contact[]> {
        return this.http
            .get('api/contacts')
            .toPromise()
            .then((response: Response) => response.json());
    }

    getContact(id: string): Promise<Contact> {
        return this.http
            .get('api/contacts/' + id)
            .toPromise()
            .then((response: Response) => response.json());
    }

    saveContact(contact: NewContact): Promise<void> {
        return this.http
            .post('api/contacts', contact)
            .toPromise()
            .then(() => <void>null);
    }

    deleteContact(id: string): Promise<void> {
        return this.http
            .delete('api/contacts/' + id)
            .toPromise()
            .then(() => <void>null);
    }

    editContact(contact: Contact): Promise<void> {
        return this.http
            .put('api/contacts/'+ contact._id, contact)
            .toPromise()
            .then(() => <void>null);
    }

    deleteManyContacts(idList: string []): Promise<void> {
        return this.http
            .post('api/contacts/delete', idList)
            .toPromise()
            .then(() => <void>null);
    }

}