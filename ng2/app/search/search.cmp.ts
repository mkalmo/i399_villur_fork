import { Component, OnInit } from '@angular/core';
import { ContactService } from '../contact.srv';
import { Contact } from "../contact.cls";

@Component({
    templateUrl: 'app/search/search.html',
    styleUrls: ['app/search/search.css']
})
export class SearchComponent implements OnInit {

    contacts: Contact[] = [];
    searchString: string = "";

    constructor(private contactService: ContactService) {}

    private updateContacts(): void {
        this.contactService.getContacts().then(contacts => this.contacts = contacts);
    }

    deleteManyContacts():void {
        this.contactService.deleteManyContacts(
            this.contacts.filter(contact => contact.done).map(contact => contact._id))
            .then(() => this.updateContacts());

    }

    deleteContact(contactId : string): void {
        this.contactService.deleteContact(contactId)
            .then(() => this.updateContacts());
    }

    ngOnInit(): void {
        this.updateContacts();
    }

}
