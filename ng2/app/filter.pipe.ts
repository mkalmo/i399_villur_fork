import {Injectable, Pipe, PipeTransform} from '@angular/core';
import {Contact} from "./contact.cls";

@Pipe({
    name: 'contactFilter'
})
@Injectable()
export class ContactFilterPipe implements PipeTransform {
    transform(contacts: Contact[], searchString: string): any {
        let result = contacts.filter(function(contact){
            if(contact.name.toString().indexOf(searchString) !== -1 ||
                contact.phone.toString().indexOf(searchString) !== -1) return true;
            return false;
        });


        return result;

    }
}