(function() {
  'use strict';

  angular.module('app').controller('newCtrl', Ctrl);

  function Ctrl($http,$window) {

    var vm = this;

    vm.item = {
      editName: '',
      editPhone: ''
    }

   

    this.save = function() {
      var editFull = {
        name: vm.item.editName,
        phone: vm.item.editPhone
      }

      
        $http.post('api/contacts', editFull).then(function(result) {
          $window.location = "#/search"


        });
      

      
    }

  }

})();