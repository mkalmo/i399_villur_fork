(function() {
  'use strict';



  angular.module('app').controller('searchCtrl', Ctrl);

  function Ctrl($http,$window,dataExchange,modalService) {
    var vm = this;
    vm.items = [];
    vm.newItem = '';
    vm.searchStr='';
    
    vm.filterData = {
      filter: 'name',
      reverse: false
    };

    function init() {
      $http.get('api/contacts').then(function(result) {
        vm.items = result.data.map(function(item) {
          item.done = false;
          item.editing = false;
          item.editName = item.name;
          item.editPhone = item.phone;
          return item;
        });

      });

    }

    init();
    
    this.filterStr = function(item) {
      
      if(vm.searchStr === ''){
        return true;
      }else{
        return (item.name.toUpperCase().includes(vm.searchStr.toUpperCase())||item.phone.toUpperCase().includes(vm.searchStr.toUpperCase()));
      }
      
      
    }

    this.filterFun = function() {
      var result;
      if (vm.filterData.reverse === true) {
        result = '-' + vm.filterData.filter;
      } else {
        result = vm.filterData.filter;
      }
      return result;
    }

    this.getClass = function(value) {
      var result = '';
      if (vm.filterData.filter === value) {
        if (vm.filterData.reverse) {
          result = 'arrow up';
        } else {
          result = 'arrow down';
        }
      } else {
        result = 'hidden';
      }
      return result;
    }

    this.filterChange = function(filter) {

      if (filter !== vm.filterData.filter) {
        vm.filterData.filter = filter;
        vm.filterData.reverse = false;
      } else {
        vm.filterData.reverse = !vm.filterData.reverse;
      }

    }

    this.delete = function(id) {

        modalService.confirm()
          .then(function () {
              return $http.delete('api/contacts/' + id);
          }).then(init);
      
      
    }

    this.edit = function(item) {
      var editData = {
        _id: item._id,
        name: item.editName,
        phone: item.editPhone
      }
      
      dataExchange.setData(editData);
      
      console.log(item);
      $window.location = "#/edit"
    }


  }

})();